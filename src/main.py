import numpy as np
import matplotlib.pyplot as plt


def trapezoid(x: float, a: float, b: float, c: float, d: float):
    if a <= x < b:
        return (x - a) / (b - a)
    if b <= x < c:
        return 1
    if c <= x < d:
        return (d - x) / (d - c)
    return 0


def triangle(x: float, a: float, b: float, c: float):
    if a <= x < b:
        return (x - a) / (b - a)
    if b <= x <= c:
        return (c - x) / (c - b)
    return 0


def mf(x: float, params):
    if len(params) == 3:
        return triangle(x, *params)
    return trapezoid(x, *params)


def fuzzy_and(*args):
    return min(args)


def fuzzy_or(*args):
    return max(args)


def fuzzy_not(x: float):
    return 1 - x


def fuzzy_aggregate(rule_outputs, points):
    return [max([min(mf(point, params), output) for (output, params) in rule_outputs]) for point in points]


def deffuz_mom(points, aggregated_values):
    max_value = max(aggregated_values)
    return np.average([point for point, value in zip(points, aggregated_values) if value >= max_value])


def defuzz_centroid(points, aggregated_values):
    return sum([point * value for point, value in zip(points, aggregated_values)]) / sum(aggregated_values)


def calculate_rules(input_values, rules, points):
    return fuzzy_aggregate([rule(**input_values) for rule in rules], points)


inputs = {
    "kodo_eilutes": {
        "mazas": (1, 1, 400, 600),
        "vidutiniskas": (400, 800, 1000, 1200),
        "didelis": (900, 1400, 2000, 2000)
    },
    "funkcijos": {
        "mazas": (1, 1, 5, 20),
        "vidutiniskas": (5, 15, 30, 45),
        "didelis": (35, 60, 100, 100)
    },
    "autoriaus_stazas": {
        "nepatyres": (0, 0, 0.5, 2.5),
        "savarankiskas": (1.5, 3, 4, 5),
        "ekspertas": (3.5, 7, 10, 10)
    }
}

outputs = {
    "klaidos": {
        "mazas": (0, 0, 20),
        "vidutiniskas": (10, 45, 70),
        "didelis": (50, 70, 100, 100)
    },
}

rules = [
    # 1
    lambda **args: (fuzzy_or(mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["didelis"]),
                             mf(args["funkcijos"], inputs["funkcijos"]["didelis"])),
                    outputs["klaidos"]["vidutiniskas"]),
    # 2
    lambda **args: (fuzzy_and(mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["didelis"]),
                              mf(args["funkcijos"], inputs["funkcijos"]["mazas"]),
                              fuzzy_not(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["ekspertas"]))),
                    outputs["klaidos"]["didelis"]),
    # 3
    lambda **args: (fuzzy_and(mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["mazas"]),
                              mf(args["funkcijos"], inputs["funkcijos"]["vidutiniskas"])),
                    outputs["klaidos"]["mazas"]),
    # 4
    lambda **args: (fuzzy_and(mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["mazas"]),
                              mf(args["funkcijos"], inputs["funkcijos"]["didelis"])),
                    outputs["klaidos"]["vidutiniskas"]),
    # 5
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["nepatyres"]),
                              mf(args["funkcijos"], inputs["funkcijos"]["didelis"])),
                    outputs["klaidos"]["didelis"]),
    # 6
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["nepatyres"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["mazas"])),
                    outputs["klaidos"]["mazas"]),
    # 7
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["nepatyres"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["vidutiniskas"])),
                    outputs["klaidos"]["didelis"]),
    # 8
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["nepatyres"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["didelis"])),
                    outputs["klaidos"]["didelis"]),
    # 9
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["savarankiskas"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["mazas"])),
                    outputs["klaidos"]["mazas"]),
    # 10
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["savarankiskas"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["vidutiniskas"])),
                    outputs["klaidos"]["vidutiniskas"]),
    # 11
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["savarankiskas"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["didelis"])),
                    outputs["klaidos"]["didelis"]),
    # 12
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["ekspertas"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["mazas"])),
                    outputs["klaidos"]["mazas"]),
    # 13
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["ekspertas"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["vidutiniskas"])),
                    outputs["klaidos"]["mazas"]),
    # 14
    lambda **args: (fuzzy_and(mf(args["autoriaus_stazas"], inputs["autoriaus_stazas"]["ekspertas"]),
                              mf(args["kodo_eilutes"], inputs["kodo_eilutes"]["didelis"])),
                    outputs["klaidos"]["vidutiniskas"]),
]

test_input = {
    "kodo_eilutes": 1500,
    "funkcijos": 60,
    "autoriaus_stazas": 0
}

print("input:", test_input)

rule_outputs = [rule(**test_input) for rule in rules]

print("rule outputs:")
for i, output in enumerate(rule_outputs):
    print(i, output)

steps = 1000.0
min_output = 0
max_output = 100
step_size = (max_output - min_output) / steps
points = np.arange(0, 100, step_size)

aggregated_values = fuzzy_aggregate(rule_outputs, points)

mom_value = deffuz_mom(points, aggregated_values)
print("mom:", mom_value)

centroid_value = defuzz_centroid(points, aggregated_values)
print("centroid:", centroid_value)

plt.plot(points, aggregated_values)

plt.show()
